'use strict';

(function(ns) {

    var _self = ns;

    ns.init = function(){
        console.log('grid-overlay init');

        var showOverlay = function(){
            return window.location.href.search('[?&]grid=true') !== -1;
        };

        if(showOverlay()) {
            $('body').addClass('show-grid');
        }
    };

    window.JAFF.events.on('pageReady', function () {
        _self.init();
    });

})(window.JAFF.gridOverlay = window.JAFF.gridOverlay || {});