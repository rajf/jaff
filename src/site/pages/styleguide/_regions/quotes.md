## Blockquotes


> "Have you ever shuffled your feet along over the carpet on a winter's evening and then quickly touched your finger to the nose of an unsuspecting friend? Did he jump when a bright spark leaped from your finger and struck him fairly on the very tip of his sensitive nasal organ?"

> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.
