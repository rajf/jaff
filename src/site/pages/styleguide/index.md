---
title: Style guide
template: styleguide
includes:
  headings: _regions/headings.md
  images: _regions/images.md
---
{% include "../../site/pages/styleguide/_regions/images.md" %}

{% include "../../site/pages/styleguide/_regions/links.md" %} 

{% include "../../site/pages/styleguide/_regions/horizontal-rules.md" %}

{% include "../../site/pages/styleguide/_regions/tables.md" %}

{% include "../../site/pages/styleguide/_regions/lists.md" %} 

{% include "../../site/pages/styleguide/_regions/quotes.md" %}

{% include "../../site/pages/styleguide/_regions/typographic-replacements.md" %}

{% include "../../site/pages/styleguide/_regions/emphasis.md" %}

{% include "../../site/pages/styleguide/_regions/code.md" %}
