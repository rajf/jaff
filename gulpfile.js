'use strict';

let browserSync = require('browser-sync').create();

const gulp = require('gulp'),
    twig = require('twig'),
    markdownIt = require('markdown-it'),
    markdownItAttrs = require('markdown-it-attrs'),
    markdownItDeflist = require('markdown-it-deflist'),
    markdownItFrontMatter = require('markdown-it-front-matter'),
    markdownItBracketedSpans = require('markdown-it-bracketed-spans'),
    path = require('path'),
    fs = require('fs'),
    jsYaml = require('js-yaml'),
    sharp = require('sharp'),
    mkdirp = require('mkdirp'),
    stringHash = require("string-hash"),

    plugins = require('gulp-load-plugins')({
        camelize: true,
        pattern: ['gulp-*', 'main-*'],
        DEBUG: false,
    });

/*
*   Config
*/
let config = {
    baseUrl                 : '/jaff',
    srcDir                  : 'src',
    tempDir                 : '.tmp',
    distDir                 : 'public',
    page: {
        defaultTemplate     :'default',
        defaultStylesheet   :'default'
    },
    twig: {
        extension           :'.twig',
        cache               : false
    },
    serverPort              : 8000,
    compileModules           : true,
    publishedPageExtension  : '.html',
    markdownItOptions : {
        html: true,
        linkify: true,
        typographer:  true,
        quotes: '“”‘’',
    }
};

config.siteDir = config.srcDir + '/site';
config.themeDir = config.srcDir + '/theme';
config.pagesDir = config.siteDir + '/pages';
config.homepage = config.pagesDir +'/home/index.md';

config.paths = {
    globalYaml: config.siteDir + '/data/global.yaml',
    pages: {
        src: [
            config.pagesDir + '/!(_*)*.md',
            config.pagesDir + '/**/!(_*)/*.md',
            '!' + config.homepage
        ]
    },
    templates: {
        src: [
            config.themeDir + '/templates'
        ]
    },
    styles: {
        src: [
            config.themeDir + '/styles/**/*.scss'
        ],
        dest: '/styles'
    },
    scripts: {
        src: [
            'node_modules/jquery/dist/jquery.js',
            config.themeDir + '/scripts/**/*.js',
            config.srcDir + '/modules/**/*.js'
        ],
        dest: '/scripts'
    },
    images: {
        src: config.themeDir + '/images/**/*',
        dest: '/images'
    },
    modules: {
        src: [
            config.srcDir + '/modules/**/*'
        ],
        dest: '/modules'
    },
    vendor: {
        src: [
        ],
        dest: '/vendor'
    },
    fonts: {
        src: [
            // 'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid*',
            // 'node_modules/@fortawesome/fontawesome-free/webfonts/fa-brand*'
        ],
        dest: '/webfonts'
    },
    extras: {
        src: [
            config.themeDir + '/extras/**/*',
        ],
        dest: '/extras'
    }
};

let page = {
    template:   config.page.defaultTemplate,
    stylesheet: config.page.defaultStylesheet
};

let twigOpts = {
    path:           config.paths.templates.src + '/',
    base:           config.paths.templates.src,
    namespaces:     {'modules': config.paths.modules.src},
};

twig.cache(config.twig.cache);

/*
 *   Functions
 */

function parseMarkDown(filePath){
    let parsedMarkdown = {};

    if (!fileExists(filePath)) {
        parsedMarkdown.content = 'parseMarkDown: ' + filePath + ' does not exist.' ;
        console.error(parsedMarkdown.content);
        return parsedMarkdown;
    }

    try {

        // parse content for twig
        twigOpts.data = fs.readFileSync(filePath, 'utf-8');
        parsedMarkdown.content = twig.twig(twigOpts).render();

        let md = new markdownIt(config.markdownItOptions)
            .use(markdownItBracketedSpans)
            .use(markdownItAttrs)
            .use(markdownItDeflist)
            .use(markdownItFrontMatter, function (fm) {
                if (fm !== null && fm !== undefined) {
                    try{
                        parsedMarkdown.meta = jsYaml.safeLoad(fm, 'utf-8');
                    }
                    catch (error){
                        console.error(error);
                    }
                }
            });

        parsedMarkdown.content = md.render(parsedMarkdown.content);

    } catch (e) {
        console.error(e);
        parsedMarkdown.content = e;
    }

    return parsedMarkdown;
}

function fromDir(startPath, extension, fileList, startingLevel, level){

    startingLevel = startingLevel || 0;
    level = level + 1 || 0;
    fileList = fileList || [];

    if (!fs.existsSync(startPath)){
        console.log("no dir ",startPath);
        return;
    }

    let files=fs.readdirSync(startPath);
    // console.log(files);

    for(let i=0;i<files.length;i++){
        let filename=path.join(startPath,files[i]);
        let stat = fs.lstatSync(filename);

        // console.log(filename.indexOf(extension));
        if (stat.isDirectory()){
            fromDir(filename,extension, fileList, startingLevel, level); //recurse
        }
        else if (filename.indexOf(extension) >= 0 && level >= startingLevel) {
            fileList.push({
                path: filename,
                directory: path.dirname(filename)
            });

            // console.log(fileList);
        }
    }

    return fileList;
}

function fileExists(path){
    if(fs.existsSync(path)) {
        return true;
    }

    console.log('NOTICE: ' + path + ' not found.');
    return false;
}

function processImage(inputFilePath, outputFilePath,  options) {

    let image = sharp(inputFilePath);

    try {
        image.resize(options[0])
            .toFile(outputFilePath)
            .catch(err => {
                console.log(err);
            });
    } catch (e){
        console.log(e);
    }

    // return '/' + path.join(outputDir, outputFileName);
}

function generatePage(inputPagePath) {
    return gulp.src(inputPagePath)
        .pipe(plugins.fn(function(file) {

            // parse global yaml
            if (fileExists(config.paths.globalYaml)) {
                page.global = jsYaml.safeLoad(fs.readFileSync(config.paths.globalYaml, 'utf-8'));
            }

            let parsedMarkdown = parseMarkDown(file.path);
            page.meta = parsedMarkdown.meta;
            page.content = parsedMarkdown.content;

            if (page.meta !== null && page.meta !== undefined) {

                if (page.meta.template !== null && page.meta.template !== undefined) {
                    page.template = page.meta.template;
                    page.stylesheet = page.meta.stylesheet;
                }
                page.includes = [];
                for (let i in page.meta.includes) {
                    const includeFilePath = path.dirname(file.path) + '/' + page.meta.includes[i];
                    const exists = fileExists(includeFilePath);
                    page.includes.push(includeFilePath)

                    // parse yaml
                    if (exists && path.extname(includeFilePath) === '.yaml') {
                        page.includes[i] = jsYaml.safeLoad(fs.readFileSync(includeFilePath, 'utf-8'));
                    }

                    // parse markdown
                    if (exists && path.extname(includeFilePath) === '.md') {
                        page.includes[i] = parseMarkDown(includeFilePath);
                    }
                }

            }

            try{
                if (fileExists(twigOpts.path)) {
                    // load template
                    twigOpts.data = fs.readFileSync(twigOpts.path + page.template + config.twig.extension, 'utf-8');
                    file.contents = new Buffer(twig.twig(twigOpts).render(page));
                }
            }
            catch (error){
                console.error(error);
            }
        }))
        .pipe(plugins.rename(function (path) {
            path.extname = ".html";
        }))
        .pipe(plugins.preprocess({context: { BASE_URL: config.baseUrl}}))
        .pipe(gulp.dest(config.tempDir))
        .pipe(plugins.size())
        .on('error', function (error) {
            console.error(error);
        });
}

twig.extendFilter('resize', function (image, options) {

    let ext = path.extname(image);
    let inputFilePath = path.join('./', config.paths.images.src.replace('/**/*', ''), image);

    // let outputDir = path.join('./',  config.paths.images.src, 'resized', path.dirname(image));
    let relativeDir = path.join(config.paths.images.dest, '_resized', path.dirname(image));
    let outputDir = path.join(config.distDir, relativeDir);
    let outputFileName = path.basename(image, ext) + '_' + stringHash(JSON.stringify(options)) + ext;
    let outputFilePath = path.join(outputDir, outputFileName);
    let relativeFilePath =  path.join(relativeDir, outputFileName);

    // console.log(inputFilePath);
    // console.log(relativeFilePath);
    // console.log(options[0].width);

    // console.log(fs.existsSync(outputDir));

    // if (!fs.existsSync(outputDir)) {
    mkdirp(outputDir, function (err) {
        if (err) {
            console.error(err);
        }
        processImage(inputFilePath, outputFilePath, options);
    });
    // }

    // console.log('relativeFilePath: ' + relativeFilePath);

    // return relativeFilePath.substring(1);
    return config.baseUrl + relativeFilePath;
});

twig.extendFilter('stripToStr', function (str, needle, bool) {
    let pos  = str.indexOf(needle);
    if (pos === -1) {
        return false
    } else {
        if (bool) {
            return str.substr(0, pos)
        } else {
            return str.slice(pos)
        }
    }
});

twig.extendFunction('sortby', function (array, orderBy, desc) {
    array.sort(function(a, b) {
        a = eval('a.' + orderBy);
        b = eval('b.' + orderBy);
        return a>b ? -1 : a<b ? 1 : 0;
    });

    if (desc) {
        array.reverse();
    }

    return array;
});

twig.extendFunction('parseSubDirMd', function (startPath, relativePath, startingLevel) {
    let fileList =  fromDir(config.srcDir + startPath, '.md', [], startingLevel);
    for (let i in fileList) {
        let filePath = fileList[i].path;
        // parse markdown
        fileList[i].url = fileList[i].directory.replace(config.srcDir + startPath, relativePath);
        fileList[i] = parseMarkDown(filePath, 'utf-8');
    }

    // fileList.sort(function(a, b) {
    //     console.log(a.meta.DOBfrom + ' : ' + b.meta.DOBfrom);
    //     a = new Date(a.DOBfrom);
    //     b = new Date(b.DOBfrom);
    //     return a>b ? -1 : a<b ? 1 : 0;
    // });

    return fileList;
});

/*
 *   Tasks
 */

gulp.task('clean', function () {
    let del = require('del');
    return del([
        config.tempDir,
        config.distDir
    ], {force:true});
});

gulp.task('legacyStyles', ['styles'], function () {
    gulp.src(config.tempDir + config.paths.styles.dest + '**/*.css')
        .pipe(plugins.noMediaQueries())
        .pipe(plugins.concat('ie.css'))
        .pipe(gulp.dest(config.tempDir + config.paths.styles.dest))
        .pipe(plugins.size())
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('styles', function () {
    let processors = [
        require('autoprefixer')({
            browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
        }),
        //require('postcss-pxtorem')({
        //    root_value: 16,
        //    unit_precision: 5,
        //    prop_white_list: [],
        //    selector_black_list: ['border-radius', 'letter-spacing'],
        //    replace: false,
        //    media_query: true
        //}),
        //require('postcss-font-magician')({
        //    hosted: '../fonts'
        //}),
        //require('postcss-normalize'),
        require('postcss-color-rgba-fallback'),
        require('postcss-pseudoelements'),
        require('postcss-opacity'),
        require('css-mqpacker')
    ];

    return gulp.src(
        config.paths.styles.src, {
            // base: config.themeDir + '/' + config.paths.styles.dest
        })
        .pipe(plugins.sassGlobImport())
        .pipe(plugins.sass({
            style: 'expanded',
            precision: 10
        })
            .on('error', plugins.sass.logError))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.postcss(processors))
        .pipe(plugins.sourcemaps.write())
        .pipe(plugins.rename({ dirname: '' }))
        .pipe(gulp.dest(config.tempDir + config.paths.styles.dest))
        .pipe(plugins.size());
});

gulp.task('scripts', function () {
    let modulesFilter = plugins.filter(['*','!' + config.paths.modules.src]);

    return gulp.src(config.paths.scripts.src)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter(require('jshint-stylish')))
        .pipe(plugins.if(!config.compileModules, modulesFilter))
        .pipe(plugins.concat('theme.js'))
        .pipe(gulp.dest(config.tempDir + config.paths.scripts.dest))
        .pipe(plugins.size())
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('vendor', function () {

    return gulp.src(config.paths.vendor.src)
        .pipe(gulp.dest(config.tempDir + config.paths.vendor.dest))
        .pipe(plugins.size())
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('modernizr', function() {
    gulp.src(config.paths.scripts.src)
        .pipe(plugins.modernizr({
            "options": [
                "setClasses",
                //"addTest",
                //"html5printshiv",
                //"testProp",
                //"fnBind"
            ],
            "tests": [
                //"setClasses",
                "flexbox",
                "csstransforms",
                "no-touch",
                "pointerevents",
                //"flexboxlegacy"
                //"addTest",
                //"html5printshiv",
                //"testProp",
                //"fnBind"
            ]
        }))
        // .pipe(plugins.uglify())
        // .pipe(plugins.rename({suffix: '.min'}))
        .pipe(gulp.dest(config.distDir + config.paths.vendor.dest))
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('homepage', function () {
    return generatePage(config.homepage, {
        base: 'home'
    });
});

gulp.task('pages', ['homepage'], function () {
    return generatePage(config.paths.pages.src);
});

gulp.task('images', function () {
    return gulp.src(config.paths.images.src)
        .pipe(gulp.dest(config.distDir + config.paths.images.dest))
        .pipe(plugins.size())
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('imageMin', ['images', 'setBaseURL'], function () {
    return gulp.src(config.distDir + config.paths.images.dest + '/**/*')
        .pipe(plugins.imagemin([
            plugins.imagemin.gifsicle({interlaced: true}),
            plugins.imagemin.jpegtran({progressive: true}),
            plugins.imagemin.optipng({optimizationLevel: 5}),
            plugins.imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(config.distDir + config.paths.images.dest))
        .pipe(plugins.size())
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('modules', function () {
    let modulesFilter = plugins.filter(['!**/*.js', '!**/*.twig', '!**/*.scss', '!**/*.sass']);

    return gulp.src(
        config.paths.modules.src, {
            base: config.srcDir + config.paths.modules.dest,
            nodir: true
        })
        .pipe(modulesFilter)
        .pipe(gulp.dest(config.distDir + config.paths.modules.dest))
        .pipe(plugins.size());
});

gulp.task('fonts', function () {
    return gulp.src(config.paths.fonts.src)
        .pipe(gulp.dest(config.tempDir + config.paths.fonts.dest))
        .pipe(gulp.dest(config.distDir + config.paths.fonts.dest))
        .pipe(plugins.size());
});

gulp.task('extras', function () {
    return gulp.src(
        config.paths.extras.src, {
            base: config.themeDir + config.paths.extras.dest,
            dot: true,
        }, { nodir: true })
        .pipe(gulp.dest(config.distDir + config.paths.extras.dest))
        .on('error', function (error) {
            console.error('' + error);
        });
});

gulp.task('pages-watch', ['pages'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('styles-watch', ['styles'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('scripts-watch', ['scripts'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('images-watch', ['images'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('extras-watch', ['extras'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('build', ['images', 'pages', 'modernizr', 'styles', 'scripts', 'vendor', 'modules', 'extras'], function (done) {
    done();
});

gulp.task('serve', ['build'], function () {

    browserSync.init({
        port: config.serverPort,
        open: 'local',
        server: {
            baseDir: [
                config.tempDir,
                config.distDir,
                './'
            ]
        }
    });

    gulp.watch([
            config.srcDir + '/**/*' + config.twig.extension,
            config.srcDir + '/**/*.yaml',
            config.srcDir + '/**/*.md'
        ],
        ['pages-watch']);
    gulp.watch(config.paths.styles.src, ['styles-watch']);
    gulp.watch(config.paths.scripts.src, ['scripts-watch']);
    gulp.watch(config.paths.images.src, ['images-watch']);
    gulp.watch(config.paths.extras.src, ['extras-watch']);

    // gulp.watch([
    //     config.tempDir + '/**/*',
    //     // config.distDir + '/**/*'
    // ])
    // .on("change", browserSync.reload)
    // .on('error', function (error) {
    //     console.error('' + error);
    //     this.emit('end');
    // });

});

// TODO: speedup 'imageMin'
gulp.task('minify', ['build'], function () {
    let jsFilter = plugins.filter('**/*.js'),
        cssFilter = plugins.filter('**/*.css'),
        htmlFilter = plugins.filter('**/*.html');

    let processors = [
        require('cssnano')
    ];

    return gulp.src(config.tempDir + '/**/*')
        .pipe(jsFilter)
        .pipe(plugins.uglify())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.postcss(processors))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(cssFilter.restore())
        .pipe(htmlFilter)
        .pipe(plugins.useref())
        .pipe(htmlFilter.restore())
        .pipe(gulp.dest(config.distDir))
        .pipe(plugins.size());
});


gulp.task('deploy', ['clean'], function () {
    config.baseUrl = '';
    gulp.start('minify');
});

gulp.task('publish', ['clean'], function () {
    gulp.start('minify');
});

gulp.task('default', ['clean'], function () {
    config.baseUrl = '';
    gulp.start('serve');
});