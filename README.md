# JAFF
## Just Another Front-end Framework

Inspired by [Assemble.io](http://assemble.io/) and other flat file content editors.

>Features:
>- Yaml for data
>- Markdown for content blocks
>- Twig for templating
>- SCSS for styles
>- Gulp for build
>- Modular Javacsript
>- Modular styles

## Install
- Run npm install

## Configuration

### src/site/pages
The structure and naming of files in this folder determines your site structure.

### src/site/data
For YAML files. YAML used for ease of editing. This is parsed to JSON in the build so could be replaces quite easily with pure JSOn files if you prefer.

### src/site/data/global.yaml
Anything in here is available to all templates. Good for menus, lists, etc. See gulp file for implimentation.

### src/site/theme
Theme files for your site.

### src/site/modules
Add-on modules.

## Custom paths
Custom paths can be configured in Gulp file.

## TODO